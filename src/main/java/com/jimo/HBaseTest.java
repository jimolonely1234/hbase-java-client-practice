package com.jimo;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;

import static com.jimo.MyUtil.printResult;

/**
 * @author wangpeng
 * @func
 * @date 2018/8/17 14:23
 */
public class HBaseTest {
    private Admin admin;
    private final String tableName = "t_blog";
    private final String cf1 = "cf1";
    private final String cf2 = "cf2";

    /**
     * title为列名
     */
    private final String title = "title";

    private Table table;

    @Before
    public void init() throws IOException {
        Connection connection = MyUtil.getConnection();
        admin = connection.getAdmin();
        table = connection.getTable(TableName.valueOf(tableName));
    }

    /**
     * @func 列出所有表
     * @author wangpeng
     * @date 2018/8/17 14:45
     */
    @Test
    public void listTables() throws IOException {
        HTableDescriptor[] hTableDescriptors = admin.listTables();
        for (HTableDescriptor descriptor : hTableDescriptors) {
            System.out.println(descriptor.getNameAsString());
        }
        /**
         homedata
         monitor120ExtendHistory
         monitor120ExtendLive
         monitor120History
         monitor120Live
         monitor15ExtendHistory
         monitor15ExtendLive
         monitor1ExtendHistory
         monitor1ExtendLive
         monitor30ExtendHistory
         monitor30ExtendLive
         monitor30History
         monitor30Live
         monitor5ExtendHistory
         monitor5ExtendLive
         monitor5ExtendLiveTest
         monitor5ExtendLiveTest2
         monitor5History
         monitor5Live
         monitor5LiveTest
         monitor5LiveTest2
         monitorDay
         monitorDayExtend
         testKfkBulkload
         */
    }

    /**
     * @func 判断表是否存在
     * @author wangpeng
     * @date 2018/8/17 14:54
     */
    public boolean isTableExists() throws IOException {
        return admin.tableExists(TableName.valueOf(tableName));
    }

    /**
     * @func 创建表
     * @author wangpeng
     * @date 2018/8/17 14:47
     */
    @Test
    public void createTable() throws IOException {
        TableName tableName = TableName.valueOf(this.tableName);
        HTableDescriptor hTableDescriptor = new HTableDescriptor(tableName);
        hTableDescriptor.addFamily(new HColumnDescriptor(cf1));
        hTableDescriptor.addFamily(new HColumnDescriptor(cf2));
        admin.createTable(hTableDescriptor);
        System.out.println(isTableExists());
    }

    /**
     * @func 禁用一张表
     * @author wangpeng
     * @date 2018/8/17 14:58
     */
    @Test
    public void disableTable() throws IOException {
        if (!admin.isTableDisabled(TableName.valueOf(tableName))) {
            admin.disableTable(TableName.valueOf(tableName));
        }
        System.out.println("已经disabled");
    }

    /**
     * @func enable一张表
     * @author wangpeng
     * @date 2018/8/17 15:00
     */
    @Test
    public void enableTable() throws IOException {
        if (!admin.isTableEnabled(TableName.valueOf(tableName))) {
            admin.enableTable(TableName.valueOf(tableName));
        }
        System.out.println("已经enabled");
    }

    /**
     * @func 加入列族
     * @author wangpeng
     * @date 2018/8/17 15:04
     */
    @Test
    public void addColumnFamilty() throws IOException {
        HColumnDescriptor title = new HColumnDescriptor("test");
        admin.addColumn(TableName.valueOf(tableName), title);
    }

    /**
     * @func 删除列族
     * @author wangpeng
     * @date 2018/8/17 15:09
     */
    @Test
    public void deleteColumnFamily() throws IOException {
        admin.deleteColumn(TableName.valueOf(tableName), Bytes.toBytes("test"));
    }

    /**
     * @func 插入数据
     * @author wangpeng
     * @date 2018/8/17 15:21
     */
    @Test
    public void putData() throws IOException {
        Put r1 = new Put(Bytes.toBytes("row-key-0"));
        r1.addColumn(Bytes.toBytes(cf1), Bytes.toBytes(title), Bytes.toBytes("java入门"));
        table.put(r1);

        /*批量插入*/
        List<Put> puts = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Put put = new Put(Bytes.toBytes("row-key-" + (i + 1)));
            put.addColumn(Bytes.toBytes(cf1), Bytes.toBytes(title), Bytes.toBytes("python教程-" + i));
            puts.add(put);
        }
        table.put(puts);
    }

    /**
     * @func 根据row key获取数据
     * @author wangpeng
     * @date 2018/8/17 15:35
     */
    @Test
    public void getData() throws IOException {
        Get get = new Get(Bytes.toBytes("row-key-0"));
        Result result = table.get(get);

        /*简单方式：已知列族和列名*/
        byte[] resultValue = result.getValue(Bytes.toBytes(cf1), Bytes.toBytes("title"));
        System.out.println(Bytes.toString(resultValue));

        /*复杂方式：全部查出来*/
        printResult(result);
        /*
         java入门
         ColumnFamily: cf1
         columnName: title:
         column-key: 1534491212818,value: java入门
         */
    }


    /**
     * @func scan表
     * @author wangpeng
     * @date 2018/8/17 15:56
     */
    @Test
    public void scanTable() throws IOException {
        ResultScanner scanner = table.getScanner(new Scan());
        for (Result result : scanner) {
            printResult(result);
        }
        scanner.close();
        /*
        ColumnFamily: cf1
        columnName: title:
        column-key: 1534491212818,value: java入门
        ColumnFamily: cf1
        columnName: title:
        column-key: 1534491212955,value: python教程-0
        ColumnFamily: cf1
        columnName: title:
        column-key: 1534491212955,value: python教程-1
        ColumnFamily: cf1
        columnName: title:
        column-key: 1534491212955,value: python教程-2
        ColumnFamily: cf1
        columnName: title:
        column-key: 1534491212955,value: python教程-3
        ColumnFamily: cf1
        columnName: title:
        column-key: 1534491212955,value: python教程-4
         */
    }

    /**
     * @func 更新：类似于覆盖数据
     * @author wangpeng
     * @date 2018/8/17 16:06
     */
    @Test
    public void updateData() throws IOException {
        Put put = new Put(Bytes.toBytes("row-key-0"));
        put.addColumn(Bytes.toBytes(cf1), Bytes.toBytes(title), Bytes.toBytes("java进阶"));
        table.put(put);
    }

    /**
     * @func 删除数据
     * @author wangpeng
     * @date 2018/8/17 16:10
     */
    @Test
    public void deleteData() throws IOException {
        Delete delete = new Delete(Bytes.toBytes("row-key-1"));
//        delete.addColumn(Bytes.toBytes(cf1), Bytes.toBytes(title));
//        delete.addFamily(Bytes.toBytes(cf1));
        table.delete(delete);
    }

    /**
     * @func 删除所有数据且不保留分割
     * @author wangpeng
     * @date 2018/8/17 16:04
     */
    @Test
    public void truncate() throws IOException {
        admin.truncateTable(TableName.valueOf(tableName), false);
    }

    /**
     * @func drop表, 记得先disable
     * @author wangpeng
     * @date 2018/8/17 15:13
     */
    @Test
    public void dropTable() throws IOException {
        admin.disableTable(TableName.valueOf(tableName));
        admin.deleteTable(TableName.valueOf(tableName));
    }

    /**
     * @func stop hbase
     * @author wangpeng
     * @date 2018/8/17 15:15
     */
    @Test
    public void shutdown() throws IOException {
        admin.shutdown();
    }

    @After
    public void close() throws IOException {
        admin.close();
        table.close();
    }
}
