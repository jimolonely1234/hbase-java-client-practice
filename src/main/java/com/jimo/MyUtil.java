package com.jimo;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.Map;
import java.util.NavigableMap;

/**
 * @author wangpeng
 * @func
 * @date 2018/8/17 22:04
 */
public class MyUtil {

    /**
     * @func 获取connection
     * @author wangpeng
     * @date 2018/8/17 22:06
     */
    public static Connection getConnection() throws IOException {
        Configuration configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.property.clientPort", "2181");
        configuration.set("hbase.zookeeper.quorum", "hadoop6,hadoop5,hadoop4");
        return ConnectionFactory.createConnection(configuration);
    }

    /**
     * @func 解析并打印Result结果
     * @author wangpeng
     * @date 2018/8/17 16:00
     */
    public static void printResult(Result result) {
        NavigableMap<byte[], NavigableMap<byte[], NavigableMap<Long, byte[]>>> navigableMap = result.getMap();
        for (Map.Entry<byte[], NavigableMap<byte[], NavigableMap<Long, byte[]>>> entry : navigableMap.entrySet()) {
            System.out.println("ColumnFamily: " + Bytes.toString(entry.getKey()));
            NavigableMap<byte[], NavigableMap<Long, byte[]>> value = entry.getValue();
            for (Map.Entry<byte[], NavigableMap<Long, byte[]>> m : value.entrySet()) {
                System.out.println("columnName: " + Bytes.toString(m.getKey()) + ":");
                NavigableMap<Long, byte[]> mValue = m.getValue();
                for (Map.Entry<Long, byte[]> mm : mValue.entrySet()) {
                    System.out.println("column-key: " + mm.getKey() + ",value: " + Bytes.toString(mm.getValue()));
                }
            }
        }
    }

    /**
     * @func 打印ResultScanner
     * @author wangpeng
     * @date 2018/8/17 22:08
     */
    public static void printResults(ResultScanner results) {
        for (Result result : results) {
            printResult(result);
        }
        results.close();
    }

    /**
     * @func 解析的另一种方法
     * @author wangpeng
     * @date 2018/8/20 8:56
     */
    public static void printResults2(ResultScanner rs) throws IOException {
        for (Result r = rs.next(); r != null; r = rs.next()) {
            for (Cell cell : r.rawCells()) {
                //TODO
            }
        }
        rs.close();
    }
}
