package com.jimo;

import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * @author wangpeng
 * @func filters test
 * @date 2018/8/17 21:47
 */
public class ScanFilterTest {
    private final String cf1 = "cf1";
    private final String cf2 = "cf2";

    /**
     * title为列名
     */
    private final String title = "title";

    private Table table;

    @Before
    public void init() throws IOException {
        Connection connection = MyUtil.getConnection();
        String tableName = "t_blog";
        table = connection.getTable(TableName.valueOf(tableName));
    }

    /**
     * @func 单一列值过滤器，有多种比较符,会获取整行数据
     * @author wangpeng
     * @date 2018/8/17 22:09
     */
    @Test
    public void singleColumnFilter() throws IOException {
        SingleColumnValueFilter filter = new SingleColumnValueFilter(
                Bytes.toBytes(cf1),
                Bytes.toBytes(title),
                CompareFilter.CompareOp.EQUAL,
                Bytes.toBytes("python教程-3")
        );
        Scan scan = new Scan();
        scan.setFilter(filter);
        ResultScanner results = table.getScanner(scan);
        MyUtil.printResults(results);
    }

    /**
     * @func 匹配单个cell
     * @author wangpeng
     * @date 2018/8/17 23:14
     */
    @Test
    public void columnFilter() {
        /*在hbase2.0中加入，作为SingleColumnFilter的补充*/
    }

    /**
     * @func 准确搜索指定的列，避免无效的搜索其他列族，简单搜索推荐
     * @author wangpeng
     * @date 2018/8/17 23:19
     */
    @Test
    public void valueFilter() throws IOException {
        ValueFilter filter = new ValueFilter(
                CompareFilter.CompareOp.EQUAL,
                new BinaryComparator(Bytes.toBytes("python教程-4"))
        );
        Scan scan = new Scan();
        scan.addColumn(Bytes.toBytes(cf1), Bytes.toBytes(title));
        scan.setFilter(filter);
        ResultScanner results = table.getScanner(scan);
        MyUtil.printResults(results);
    }

    /**
     * @func 正则过滤, 凡是以python开头的都可以，参考：
     * https://docs.oracle.com/javase/6/docs/api/java/util/regex/Pattern.html
     * @author wangpeng
     * @date 2018/8/20 8:14
     */
    @Test
    public void regexFilter() throws IOException {
        RegexStringComparator comparator = new RegexStringComparator("python.");
        SingleColumnValueFilter singleColumnValueFilter = new SingleColumnValueFilter(
                Bytes.toBytes(cf1),
                Bytes.toBytes(title),
                CompareFilter.CompareOp.EQUAL,
                comparator
        );
        Scan scan = new Scan();
        scan.setFilter(singleColumnValueFilter);
        ResultScanner results = table.getScanner(scan);
        MyUtil.printResults(results);
    }

    /**
     * @func 子串匹配
     * @author wangpeng
     * @date 2018/8/20 8:26
     */
    @Test
    public void subStringFilter() throws IOException {
        /*匹配 xxx教程-2xxx*/
        SubstringComparator comparator = new SubstringComparator("教程-2");
        SingleColumnValueFilter singleColumnValueFilter = new SingleColumnValueFilter(
                Bytes.toBytes(cf1),
                Bytes.toBytes(title),
                CompareFilter.CompareOp.EQUAL,
                comparator
        );
        Scan scan = new Scan();
        scan.setFilter(singleColumnValueFilter);
        ResultScanner results = table.getScanner(scan);
        MyUtil.printResults(results);
        /*
        ColumnFamily: cf1
        columnName: title:
        column-key: 1534491212955,value: python教程-2
         */
    }

    /**
     * @func 前缀匹配
     * @author wangpeng
     * @date 2018/8/20 8:42
     */
    @Test
    public void binaryPrefixFilter() throws IOException {
        BinaryPrefixComparator comparator = new BinaryPrefixComparator(Bytes.toBytes("python"));
        SingleColumnValueFilter singleColumnValueFilter = new SingleColumnValueFilter(
                Bytes.toBytes(cf1),
                Bytes.toBytes(title),
                CompareFilter.CompareOp.EQUAL,
                comparator
        );
        Scan scan = new Scan();
        scan.setFilter(singleColumnValueFilter);
        ResultScanner results = table.getScanner(scan);
        MyUtil.printResults(results);
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/20 8:48
     */
    @Test
    public void columnPrefixFilter() throws IOException {
        ColumnPrefixFilter filter = new ColumnPrefixFilter(Bytes.toBytes("tit"));
        Scan scan = new Scan();
        scan.addFamily(Bytes.toBytes(cf1));
        scan.setFilter(filter);
        scan.setBatch(10);
        ResultScanner rs = table.getScanner(scan);
        MyUtil.printResults(rs);
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/20 9:00
     */
    @Test
    public void multiColumnPrefixFilter() throws IOException {
        byte[][] prefix = {Bytes.toBytes("my-column"), Bytes.toBytes("title")};
        MultipleColumnPrefixFilter filter = new MultipleColumnPrefixFilter(prefix);
        Scan scan = new Scan();
        scan.addFamily(Bytes.toBytes(cf1));
        scan.setFilter(filter);
        scan.setBatch(10);
        ResultScanner rs = table.getScanner(scan);
        MyUtil.printResults(rs);
    }

    /**
     * @func 匹配指定范围的列名，这里只有一个title列名
     * @author wangpeng
     * @date 2018/8/20 9:12
     */
    @Test
    public void columnRangeFilter() throws IOException {
        ColumnRangeFilter filter = new ColumnRangeFilter(
                Bytes.toBytes("titld"), true,
                Bytes.toBytes("titlf"), true
        );
        Scan scan = new Scan();
        scan.setFilter(filter);
        scan.addFamily(Bytes.toBytes(cf1));
        ResultScanner scanner = table.getScanner(scan);
        MyUtil.printResults(scanner);
    }

    /**
     * @func 行过滤器，可以加各种Comparator
     * @author wangpeng
     * @date 2018/8/20 9:18
     */
    @Test
    public void rowFilter() throws IOException {
        /*>=3的行,可以使用FilterList组合多个界限*/
        RowFilter beginRowFilter = new RowFilter(
                CompareFilter.CompareOp.GREATER_OR_EQUAL,
                new BinaryComparator(Bytes.toBytes(3))
        );
        Scan scan = new Scan();
        /*等价于：Scan scan = new Scan(Bytes.toBytes(1), Bytes.toBytes(10));*/
        scan.setFilter(beginRowFilter);
        scan.addFamily(Bytes.toBytes(cf1));
        ResultScanner scanner = table.getScanner(scan);
        MyUtil.printResults(scanner);
    }


    /**
     * @func 过滤器列表
     * @author wangpeng
     * @date 2018/8/17 21:51
     */
    @Test
    public void filterList() throws IOException {
        FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
//        new FilterList(FilterList.Operator.MUST_PASS_ALL);

        SingleColumnValueFilter filter1 = new SingleColumnValueFilter(
                Bytes.toBytes(cf1),
                Bytes.toBytes(title),
                CompareFilter.CompareOp.EQUAL,
                Bytes.toBytes("python教程2")
        );
        SingleColumnValueFilter filter2 = new SingleColumnValueFilter(
                Bytes.toBytes(cf1),
                Bytes.toBytes(title),
                CompareFilter.CompareOp.EQUAL,
                Bytes.toBytes("python教程3")
        );
        filterList.addFilter(filter1);
        filterList.addFilter(filter2);

        Scan scan = new Scan();
        scan.setFilter(filterList);
        ResultScanner results = table.getScanner(scan);
        MyUtil.printResults(results);
    }

    /**
     * @func 只取每一行的第一个KV，一般用于row count，如下(但这并不适合大表)
     * @author wangpeng
     * @date 2018/8/20 9:36
     */
    @Test
    public void firstKeyOnlyFilter() throws IOException {
        Scan scan = new Scan();
        scan.setFilter(new FirstKeyOnlyFilter());
        ResultScanner resultScanner = table.getScanner(scan);
        int totalRow = 0;
        for (Result result : resultScanner) {
            totalRow += result.size();
        }
        System.out.println("total rows: " + totalRow);
    }

    /**
     * @func 限制页数或用于分页
     * @author wangpeng
     * @date 2018/8/20 10:34
     */
    @Test
    public void pageFilter() throws IOException {
        PageFilter pageFilter = new PageFilter(2);
        Scan scan = new Scan();
        scan.setStartRow(Bytes.toBytes("row-key-2"));
        scan.setStopRow(Bytes.toBytes("row-key-5"));
        scan.setFilter(pageFilter);
        ResultScanner results = table.getScanner(scan);
        for (Result result : results) {
            System.out.println(Bytes.toString(result.getRow()));
        }
    }

    @After
    public void close() throws IOException {
        table.close();
    }
}
